package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import blackjack.Croupier;
import blackjack.Spieler;
import client.RemoteSpiel;

/*
 * Autor: Sergej Makarov
 * Universitaet Leipzig, 2017
 */

public class Spiel extends PortableRemoteObject implements RemoteSpiel{

	int wonGames = 0;
	int loseGames = 0;
	int drawnGames = 0;

	private Croupier croupier;
	private Set<Spieler> spieler = new HashSet<>(); 
	private Map<String, Spieler> lonelyGamblers = new HashMap<>();
	private Queue<String> karten;

	public Spiel() throws RemoteException {}

	@Override public boolean isLonelyClubHere() {
		return lonelyGamblers.isEmpty();
	}

	@Override
	public String getNamesOfGamblers() throws RemoteException {
		return spieler.stream().map(gambler -> gambler.getName()).reduce((str1, str2) -> str1 + ", " + str2).get();
	}

	@Override 
	public void joinTheGame(String name) throws RemoteException {
		Spieler gambler = new Spieler(name);
		gambler.addCard(karten.poll());
		gambler.addCard(karten.poll());
		spieler.add(gambler);
	}

	@Override 
	public String lonelyClub(String name) throws RemoteException {
		croupier = new Croupier();
		lonelyGamblers.put(name, new Spieler(name));
		for (int i = 0; i < 4; i ++) {
			if (i % 2 == 0) this.croupier.addCard(karten.poll());
			else lonelyGamblers.get(name).addCard(karten.poll());
		}
		return "Hello, " + name + "! Nice to have a chance to play with you! Here are your first 2 Cards: " + 
		lonelyGamblers.get(name).getCards() + 
		" and open card of Croupier is: " +
		croupier.getOpenCard();
	}

	@Override
	public void startGameWith(String name) throws RemoteException {
		spieler.add(new Spieler(name));
		croupier = new Croupier();
		karten = this.croupier.shuffle();
		for (int i = 0; i < 4; i ++) {
			if (i % 2 == 0) this.croupier.addCard(karten.poll());
			else spieler.iterator().next().addCard(karten.poll());
		}
	}

	@Override public String initialNotify(String name) {
		String gambler_cards = spieler.stream().map(gambler -> gambler.getName() + ": " + gambler.getCards()).reduce( (a, b) -> a + ", " + b).get();
		return "Here are your first 2 Cards: " + 
		gambler_cards + 
		" and open card of Croupier is: " +
		croupier.getOpenCard();
	}

	@Override
	public String addOneCardFor(String name) throws RemoteException {
		String card = karten.poll();
		Spieler gamer = null;
		//search in lonelyClub
		if (lonelyGamblers.containsKey(name)) gamer = lonelyGamblers.get(name);
		//search in gemeinsamSpiel-Tisch
		else gamer = spieler.stream().filter(g -> g.getName().equals(name)).findFirst().get();
		gamer.addCard(card);
		return "Here you are: " + card + ". Your cards are: " + gamer.getCards();
	}

	@Override
	public boolean isLooser(String name) throws RemoteException {
		Spieler gambler = null;
		if (lonelyGamblers.containsKey(name)) gambler = lonelyGamblers.get(name);
		else gambler = spieler.stream().filter(sp -> sp.getName().equals(name)).findFirst().get();
		return getValues(gambler.getCards()) > 21 ? true : false;
	}

	@Override
	public String result(String name) throws RemoteException {
		String won = "";
		List<String> results = new ArrayList<>();
		int croupier = getValues(this.croupier.getOwnCards());	
		if (lonelyGamblers.containsKey(name)) {
			int gamer_res = getValues(lonelyGamblers.get(name).getCards());
			if (!(gamer_res > 21)) {
				if (croupier > gamer_res) { won = "Croupier"; wonGames += 1; }
				else if (croupier == gamer_res) { won = "Draw Game"; drawnGames += 1; }
				else { won = lonelyGamblers.get(name).getName(); loseGames += 1;}
			} else won = "Croupier";
			lonelyGamblers.remove(name);
		} else {
			TreeMap<Integer, List<String>> winner = new TreeMap<>();
			winner.put(croupier, new ArrayList<>());
			winner.get(croupier).add("Croupier");
			for (Spieler gamer : spieler) {
				int res = getValues(gamer.getCards());
				results.add(gamer.getName() + ": " + res);
				if (!(res > 21)) {
					if (winner.containsKey(res)) winner.get(res).add(gamer.getName());
					else {
						winner.put(res, new ArrayList<>());
						winner.get(res).add(gamer.getName());
					}
				}
			}
			//endlich remove THE spieler from the Set, so that you can start a new Game without reset the Server-Stub-App;
			spieler.remove(spieler.stream().filter(sp -> sp.getName().equals(name)).findFirst().get());
			//Anwenden descendingMap() zum NavigableMap, sodass der erste Wert der/die Winner mit höchster Anzahl (aber nicht größer als 21) ist/sind
			NavigableMap<Integer, List<String>> wdesc = winner.descendingMap();
			List<String> firstValue = wdesc.firstEntry().getValue();
			if (firstValue.size() > 1) {
				won = "unentschieden";
				if (firstValue.contains("Croupier")) this.drawnGames += 1;
			} else {
				won = wdesc.firstEntry().getValue().toString();
				if (firstValue.contains("Croupier")) this.wonGames += 1;
				else loseGames += 1;
			} 
		}
		return "Result: Croupier = " + croupier + ", Gambler = " + results + 
				"\n" + "Winner is: " + won +
				"\n + some statistic of all games played this night: " + 
				"\n Croupier won: " + wonGames + " times" +
				"\n Croupier lose: " + loseGames + " times" +
				"\n Croupier has: " + drawnGames + " drawn games";
	}

	@Override
	public int getValues(List<String> ownCards) {
		int value = 0;
		List<List<String>> aceValues = new ArrayList<>();
		for (String card : ownCards) {
			String wert = card.split(" ")[0];
			System.out.println(wert);
			switch (wert) {
			case "Bube": value += 10; break;
			case "Koenig": value += 10; break;
			case "Dame": value += 10; break;
			case "ASS": aceValues.add(Arrays.asList("1", "11"));break;
			default: value += Integer.valueOf(wert); break;//if 2-10
			}
		}
		if (!aceValues.isEmpty()) {
			if (value <= 10 && aceValues.size() == 1) value += 11;
			else value += 1 * aceValues.size();
		}
		return value;
	}

	@Override
	public boolean areThereOtherGamblers() throws RemoteException {
		return spieler.size() >= 1;
	}

	public static void main(String[] args) throws RemoteException {

		int port = Integer.valueOf(args[0]);
		String objName = "BlackJack";
		Spiel obj = new Spiel();

		if(System.getSecurityManager() == null) System.setSecurityManager(new SecurityManager());

		Registry registry = LocateRegistry.getRegistry(port);
		InitialContext INC = null;
		boolean bound = false;
		for (int i = 0; !bound && i < 2; i ++)
			try {
				INC = new InitialContext();
				INC.rebind(objName,  obj);
				bound = true;
				System.out.println(objName +  " bound to registry, port " + port + ".");
			} catch (NamingException e) {
				System.out.println("Rebinding " + objName + " failed, " + "retrying ...");
				registry = LocateRegistry.createRegistry(port);
				System.out.println("Registry started on port " + port + ".");
			}

	}

}
