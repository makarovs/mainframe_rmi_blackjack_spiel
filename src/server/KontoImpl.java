package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import client.Konto;

public class KontoImpl extends PortableRemoteObject implements Konto{
	int kontostand;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	String transaction = "";
	
	protected KontoImpl() throws RemoteException {
		this.kontostand = 1000;
	}
	
	@Override
	public String getTransaction() throws RemoteException {
		return transaction;
	}

	@Override
	public String getKontostand() throws RemoteException {
		return "Der aktuelle Kontostand ist " + kontostand;
	}

	@Override
	public void einzahlung(int betrag) throws RemoteException {
		transaction = "Neue Einzahlung in Höhe von " + betrag;
		kontostand += betrag;
	}

	@Override
	public void auszahlung(int betrag) throws RemoteException {
		if (kontostand < betrag) {
			transaction = "Sorry, die Auszahlung in Hoehe von " + betrag + " ist nicht moeglich. ";
		} else {
			transaction = "Ausgezahlt am " + dateFormat.format(new Date()) + " in  Höhe von " + betrag;
			kontostand -= betrag;
		}

	}
	
	public static void main(String[] args) throws RemoteException{
		int port = (args.length > 0) ? Integer.parseInt(args[0]) : 2015;
		KontoImpl obj = new KontoImpl();
		String objName = "KontoObj";
		
		if(System.getSecurityManager() == null) System.setSecurityManager(new SecurityManager());
		
		Registry registry = LocateRegistry.getRegistry(port);
		InitialContext INC = null;
		boolean bound = false;
		for (int i = 0; !bound && i < 2; i ++)
			try {
				//registry.rebind(objName, obj);
				INC = new InitialContext();
				INC.rebind(objName,  obj);
				bound = true;
				System.out.println(objName +  "bound to registry, port " + port + ".");
			} catch (NamingException e) {
				System.out.println("Rebinding " + objName + " failed, " + "retrying ...");
				registry = LocateRegistry.createRegistry(port);
				System.out.println("Registry started on port " + port + ".");
			}
	}

}
