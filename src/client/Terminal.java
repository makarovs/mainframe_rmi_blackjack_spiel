package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Terminal {
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		String host = (args.length < 1) ? "localhost" : args[0];
		int port = (args.length < 2) ? 2015 : Integer.parseInt(args[1]);
		InitialContext INC = null;
		try {
			if (System.getSecurityManager() == null) {
				System.setSecurityManager(new SecurityManager());
			}
			INC = new InitialContext();
			//Konto obj = (Konto) Naming.lookup("rmi://" + host + ":" + port + "/" + "KontoObj");
			Konto obj = (Konto) INC.lookup("iiop://" + host + ":" + port + "/" + "KontoObj");
			System.out.println(obj.getKontostand());
			obj.einzahlung(70);
			System.out.println(obj.getTransaction());
			System.out.println(obj.getKontostand());
			obj.einzahlung(799);
			System.out.println(obj.getTransaction());
			System.out.println(obj.getKontostand());
			obj.auszahlung(1000);
			System.out.println(obj.getTransaction());
			System.out.println(obj.getKontostand());
			obj.auszahlung(2000);
			System.out.println(obj.getTransaction());
			System.out.println(obj.getKontostand());
		} catch(NamingException e) {
			System.out.println("Client failed, caugth expression " + e.getMessage());
		}
	}
}
