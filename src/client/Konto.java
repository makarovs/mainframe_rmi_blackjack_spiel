package client;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Konto extends Remote {
	public String getKontostand() throws RemoteException;
	public void einzahlung(int betrag) throws RemoteException;
	public void auszahlung(int betrag) throws RemoteException;
	public String getTransaction() throws RemoteException;
}
