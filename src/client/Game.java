package client;

import java.rmi.RemoteException;
import java.util.Scanner;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import blackjack.Spieler;

/*
 * Autor: Sergej Makarov
 * Universitaet Leipzig, 2017
 */

public class Game {

	public static void main(String[] args) throws RemoteException {
		String host = "localhost";
		int port = Integer.valueOf(args[0]);
		InitialContext INC = null;
		try {
			//Einstellungen
			if (System.getSecurityManager() == null) System.setSecurityManager(new SecurityManager());
			INC = new InitialContext();
			RemoteSpiel obj = (RemoteSpiel) INC.lookup("iiop://" + host + ":" + port + "/" + "BlackJack");
			//Spiel 
			Scanner reader = new Scanner(System.in);
			System.out.println("Hello! Your are trying to enter the gentlemen's Club for playing BlackJack!"
					+ " \n Please, tell us your name firstly!");
			String name = reader.nextLine();
			//Die Entscheidung hinsichtlich des Standalone- oder Group-Spiels
			if (obj.areThereOtherGamblers()) {
				System.out.println("There is the table with: " + obj.getNamesOfGamblers() + 
						"\n Would you like to join the gamblers or you would like alone vs. Croupier? [join/alone]");
				String answer1 = reader.nextLine();
				if (answer1.equals("join")) {
					//join the Spieler, der ein Spiel mit startGameWith() initialisiert
					System.out.println("Ok. Joining the game...");
					obj.joinTheGame(name);
					System.out.println(obj.initialNotify(name));
				} else {
					//ganz unabhängiges Spiel von den anderen Spielern (es kann mehrere Spieler im Klub sein)
					System.out.println(name + " vs. Croupier");
					System.out.println(obj.lonelyClub(name));
				}
			} else {
				//Wenn keiner Spieler gerade spielt, dann der Spieler initialisiert ein Tisch, an dem die andere Spieler mitspielen können 
				System.out.println("You are the first in the gentelmen's club. Let's get started! ");
				System.out.println(name + " vs. Croupier");
				obj.startGameWith(name);
				String start = obj.initialNotify(name);
				System.out.println(start);
			}
			System.out.println("Would you like one more card? (no/yes)");
			String answer = reader.nextLine();
			while (!answer.equals("no")) {
				System.out.println(obj.addOneCardFor(name));
				if (obj.isLooser(name)) {
					System.out.println("Sorry, you lose the game..."); break;
				}
				System.out.println("One more?");
				answer = reader.nextLine();
			}
			System.out.println(obj.result(name));
		} catch (NamingException ex) { ex.getMessage();}
	}
}
