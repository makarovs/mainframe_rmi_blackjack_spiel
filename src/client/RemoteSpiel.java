package client;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/*
 * Autor: Sergej Makarov
 * Universitaet Leipzig, 2017
 */

public interface RemoteSpiel extends Remote{
	public void startGameWith(String name) throws RemoteException;
	public void joinTheGame(String name) throws RemoteException;
	public String lonelyClub(String name) throws RemoteException;
	public String addOneCardFor(String name) throws RemoteException;
	public String result(String name) throws RemoteException;
	public int getValues(List<String> cards) throws RemoteException;
	public boolean areThereOtherGamblers() throws RemoteException;
	public String getNamesOfGamblers() throws RemoteException;
	public String initialNotify(String name) throws RemoteException;
	public boolean isLooser(String name) throws RemoteException;
	public boolean isLonelyClubHere() throws RemoteException;
}
