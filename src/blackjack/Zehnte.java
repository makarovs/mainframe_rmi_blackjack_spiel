package blackjack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Zehnte extends HashMap<String, List<String>> implements Karte {
	
	private List<String> symbols = Arrays.asList("Treff", "Pik", "Herz", "Karo");
	private List<String> values = Arrays.asList("Koenig", "Dame", "Bube");

	public Zehnte() {
		values.forEach(x -> {
			this.put(x, symbols);
		});
	}
	
	@Override
	public String getOne() {
		return this.keySet().toArray()[new Random().nextInt(3)] + " von " + symbols.get(new Random().nextInt(4));
	}
	
}
