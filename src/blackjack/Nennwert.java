package blackjack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Nennwert extends HashMap<Integer, List<String>> implements Karte{
	
	private List<String> symbols = Arrays.asList("Treff", "Pik", "Herz", "Karo");
	private List<Integer> values = Arrays.asList(2,3,4,5,6,7,8,9,10);
	
	public Nennwert() {
		values.forEach(x -> {
			this.put(x, symbols);
		});
	}

	@Override
	public String getOne() {
		String card = this.keySet().toArray()[new Random().nextInt(8) + 1] + " von " + symbols.get(new Random().nextInt(4));
		return card;
	}

}
