package blackjack;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

public class Croupier {

	private String name = "Croupier";
	private List<String> ownCards = new ArrayList<>();
	
	public Croupier() {
	}
	
	public String getOpenCard() {
		return ownCards.get(1);
	}
	public String getName() {
		return this.name;
	}
	public void addCard(String card) {
		ownCards.add(card);
	}
	public List<String> getOwnCards() {
		return this.ownCards;
	}
	public Queue<String> shuffle() {
		List<Karte> coll = Arrays.asList(new Zehnte(), new Ass(), new Nennwert());
		Queue<String> stap = new ArrayDeque<>();
		Set<String> stapel = new LinkedHashSet<>();
		//soll eigentlich 52 sein, aber wegen der Komplexität von HashSet, zu lange dauernt, dies zu berechnen
		while(stapel.size() != 48) {
			stapel.add(coll.get(new Random().nextInt(3)).getOne());
		}
		stapel.forEach(karte -> stap.add(karte));
		return stap;
	}
}
