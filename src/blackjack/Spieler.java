package blackjack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Spieler {

	private String name;
	private List<String> karten = new ArrayList<>();
	
	public Spieler(String name) {
		this.name = name;
	}
	
	public Spieler() {};
	
	public List<String> getCards() {
		return this.karten;
	}
	
	public void addCard(String card) {
		this.karten.add(card);
	}
	
	public String getName() {
		return this.name;
	}
		
}
