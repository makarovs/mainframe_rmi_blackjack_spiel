package blackjack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Ass extends HashMap<Enum, List<String>> implements Karte{
	private enum Ace {
		ASS 
	}
	public Ass() {
		this.put(Ace.ASS, Arrays.asList("Kreuz", "Pik", "Herz", "Karo"));
	}

	@Override
	public String getOne() {
		return this.keySet().toArray()[0] + " von " + this.get(Ace.ASS).get(new Random().nextInt(4));
	}
	
}
