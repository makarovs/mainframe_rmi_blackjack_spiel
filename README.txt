Erstellung von BlackJack-Spiel anhand von Java und RMI_IIOP im Rahmen des Mainframe_Intergration Moduls an der Universitaet Leipzig.

Autor: Sergej Makarov 
Uni-Leipzig, 2017

Beschreibung des BlackJack-Spiels:
In dem virtuellen Klub gibt es einen gemeinsamen Tisch und mehrere Tische für das Spiel 1:1 (gegen Croupier ohne weitere Spieler).

Ablauf des Spiels:
Wenn du der erste Spieler in dem BlackJack-Klub bist, nimmst du den gemeinsamen Tisch und fängst das Spiel gegen Croupier an, wobei die weiteren Ankömmlingen mitspielen dürfen.
Allerdings werden die weiteren Gäste des Klubs danach gefragt, ob sie an den gemeinsamen Tisch mitspielen wollen, oder spielen sie 
einfach gegen Croupier allein. In dem letzten Fall dürfen die anderen Gäste die Lonely-Club Spieler nicht stören. 

